<?php

class Crawler{
    private $link;
    protected $publications = array();
    
    function __construct($link){
        $this->link = $link;
    }
    
    function crawlLink(){
        $b = 20;
        
        $scholarHTML = file_get_html($this->link);
        while (!($scholarHTML->find('td[class=gsc_a_e]'))){
            foreach ($scholarHTML->find('tr[class=gsc_a_tr]') as $element):
                
                // First td (contains publication, author(s) and journal)
                foreach($element->find('td[class=gsc_a_t]') as $first):
                    foreach($first->find('a[class=gsc_a_at]') as $firstA):
                        $publicationName =  $firstA->innertext;
                    endforeach;
                    foreach($first->find('div[class=gs_gray]') as $firstDiv):
                        $fir = $firstDiv->find('span[class=gs_oph]', 0);
                        if($fir){ $journal = $firstDiv->innertext;}
                    endforeach;
                endforeach;

                // Second td (contains Citation Count)
                foreach($element->find('td[class=gsc_a_c]') as $second):
                    foreach($second->find('a[class=gsc_a_ac]') as $secondA):
                        $citationCount = empty($secondA->innertext)? "0":$secondA->innertext;                        
                    endforeach;
                endforeach;
                $this->publications[] = array("publicationName"=>$publicationName,
                                        "publicationJournal"=>$journal,
                                        "citationCount"=>intval($citationCount));
            endforeach;
            $addToLink = '&cstart='.$b.'&pagesize=20';            
            $link2 = $this->link.$addToLink;
            $scholarHTML = file_get_html($link2);
            $b = $b + 20;
        }
        return $this->publications;
    }
}

class Scholar{
    protected $scholarid;
    private $scholarname;
    private $scholarurl;
    
    function setScholarID($scholarid){
        $this->scholarid = $scholarid;
    }
    
    function getScholarID(){
        return $this->scholarid;
    }
    
    function getScholars() {
        $Object = new DB2Connection();

        try {
            $result = $Object->procCall('getScholars', "");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
    
    function getScholar(){
        $Object = new DB2Connection();

        try {
            $result = $Object->procCall('getScholar', "'$this->scholarid'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
        
    }
    
    function addScholar($scholarname, $scholarurl) {
        
        $this->scholarname = $scholarname;
        $this->scholarurl = $scholarurl;
        
        $Object = new DB2Connection();
        $outparam = "outparam";
        try {
            $result = $Object->procCall2('addScholar', "'$this->scholarname','$this->scholarurl',?",$outparam);
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
    
    function getScholarArticles(){        
        $Object = new DB2Connection();

        try {
            $result = $Object->procCall('getScholarArticles', "'$this->scholarid'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
        
    function compare(){
        $Object = new DB2Connection();

        try {
            $Object->procCall('compareArticles', "'$this->scholarid'");
            $result = "comparism success";
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
    
    function get30DaysDynamicsCount(){
        
        $Object = new DB2Connection();
        try {
            $result = $Object->procCall('get30DaysDynamicsCount', "'$this->scholarid'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }

    function get30DaysDynamicsDetails(){        
        $Object = new DB2Connection();
        try {
            $result = $Object->procCall('get30DaysDynamicDetails', "'$this->scholarid'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
        
    function get30DaysMostCited(){        
        $Object = new DB2Connection();
        try {
            $result = $Object->procCall('get30DaysMostCited', "'$this->scholarid'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
}

class Alert{
    private $scholarid;
    private $email;
    private $scholarname;
    
    function __construct($scholarid,$email) {
        $this->scholarid = $scholarid;
        $this->email = $email;
        $scholar = new Scholar();
        $scholar->setScholarID($this->scholarid);
        $scholarInfo = $scholar->getScholar();
        foreach($scholarInfo as $eachscholar):
            $this->scholarname = $eachscholar['SCHOLARNAME'];
        endforeach;
    
    }    
    
    private function subscriptionMail(){
            require '../PHPMailer/PHPMailerAutoload.php';

            $mail = new PHPMailer;

            $mail->isSMTP();                                        // Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com;';                        // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                                 // Enable SMTP authentication
            $mail->Username = 'gscholarbot@gmail.com';                 // SMTP username
            $mail->Password = 'quenlattie720@gmail.com';                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

            $mail->From = 'gscholarbot@gmail.com';
            $mail->FromName = 'Scholarbot Subscription';
            $mail->addAddress($this->email);                              // Add a recipient

            $mail->WordWrap = 50; 
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Citations Dynamics Tracking application';
            $mail->Body   = '<br/>You have subscribed to monthly overview of <b>'. $this->scholarname .'</b>.<br/>  '
                    . 'Please confirm your subscription by clicking on the following link'
                    . ' <a href="fyp/?action=confirm&email='. $this->email.'&scholar='.$this->scholarid.'">'
                    . 'confirm Subscription</a>.<br/> <br/> You do not need to perform any action if you did not subscribe.';
            return $mail->send();
    }
    
    function addAlert(){
        
        $Object = new DB2Connection();
        try { 
            
            if($this->subscriptionMail()) {
                $Object->procCall('addAlert', "'$this->scholarid', '$this->email'");
                $result = "Thanks for Subscribing. Confirm Subscription by following the link in your email.";     
            } else {
                $result = 'Message could not be sent.';                           
            }

        } catch (exception $e) {
            $result = $e->getMessage();
        }
    return $result;
    }
    
    function deleteAlert(){
        $Object = new DB2Connection();
        try {
            $Object->procCall('deleteAlert', "'$this->scholarid', '$this->email'");
            $result = "You have successfully unsubscribed to the monthly overview of ".$this->scholarname." .";
        } catch (exception $e) {
            $result = "Your Subscription could not be deleted: ".$e->getMessage();
        }
        return $result; 
    }
    
    function confirmSubscription(){
        $Object = new DB2Connection();
        try {
            $Object->procCall('confirmsubscription', "'$this->scholarid', '$this->email'");
            $result = "You have successfully subscribed to the monthly overview of ".$this->scholarname." .";
        } catch (exception $e) {
            $result = "You are already subscribed";
        }
        return $result; 
    }
    
    function monthlyOverviewMail(){
        
    }
    

}

class Article extends Scholar{
    private $articleid;
    private $articlename;
    private $journal;
    private $citation;
    
    
    function setArticleID($articleid){
        $this->articleid = $articleid;
    }
    
    function getArticleID(){
        return $this->articleid;
    }
    
    function setArticleDetails($articlename,$journal,$citation){
        $this->articlename = $articlename;
        $this->journal = $journal;
        $this->citation = $citation;
    }
    
    function addArticle() {
        $Object = new DB2Connection();

        try {
            $result = $Object->procCall('addArticle', "'$this->scholarid','$this->articlename','$this->journal','$this->citation'");
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
    
    function addTempArticle(){       
        $Object = new DB2Connection();

        try {
            $Object->procCall('addTempArticle', "'$this->scholarid','$this->articlename','$this->journal','$this->citation'");
            $result = "success";
        } catch (exception $e) {
            $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
        }
        return $result;
    }
    
    function getArticleDynamics(){
       $Object = new DB2Connection();
        try {
            $result = $Object->procCall('getArticleDynamics2', "'$this->articleid'");
        } catch (exception $e) {
            $result = $e->getMessage();
        }
        return $result; 
    }
}
?>