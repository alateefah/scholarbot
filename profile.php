<?php

include("inc/simple_html_dom.php");
include ("connection/DB2Connection.php");
include ("class/class.crawler.php");

if(!isset($_POST['scholarid'])):
    header("location:index.php");
endif;

$scholarID = $_POST['scholarid'];

$scholar = new Scholar();
$scholar->setScholarID($scholarID);

$ObjScholar = $scholar->getScholar();
$ObjScholarArticles = $scholar->getScholarArticles();
//var_dump($ObjScholarArticles);

include 'inc/header.inc.php';
?>
<?php if(in_array("error_msg", $ObjScholarArticles)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjScholarArticles["error_msg"]; ?></div>
<?php endif;?>
<div class="clearfix">&nbsp;</div>
<div class="col-md-10" style="border-right: 1px solid #ccc; margin-bottom: 5px;">
    <ul class="nav nav-tabs nav-justified" role="tablist" data-tabs="tabs">
        <li role="presentation" class="active">
            <a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab" class="tab-style">
                <strong><div id="count"></div></strong>
                <span class="content">
                    <em>Publications Cited</em><br/>
                    Last 30 days
                </span>
            </a>
        </li>
        <li role="presentation">
            <a href="#tab2" aria-controls="tab1" role="tab" data-toggle="tab" class="tab-style">
                <strong>10</strong>
                <span class="content">
                    <em>Top Cited Publications</em><br/>
                </span>
            </a>
        </li>
        <li role="presentation">
            <a href="#tab3" aria-controls="tab1" role="tab" data-toggle="tab" class="tab-style">
                <strong>1</strong>
                <span class="content">
                    <em>New Publication</em><br/>
                    Last 90 days
                </span>
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <div id="30daysDetails" class="chartdiv"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <div id="mostCited" class="chartdiv"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab3">
            No New Article, sorry :(
        </div>
    </div>
</div>

<div class="col-md-2">
    <?php foreach($ObjScholar as $scholar): ?>
       <h2><?php echo $scholar['SCHOLARNAME']; ?></h2>
    <?php endforeach; ?>
    <div class="btn-group">
        <button type="button" class="btn btn-primary"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Follow</button>
        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>            
        </button>       
        <div class="dropdown-menu">
            <form class="form-horizontal" id="subs" method="GET" action="ajax/ajaxGET.php">
                <div id="thanks" style="color: green; border: 1px solid #ccc; border-radius: 5px;"></div>
                <h5>Monthly Overview</h5>
                <div class="form-group" style="margin-left: 0px; ">
                    <div class="input-group">
                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" required="required">
                    </div>
                </div>
                <button type="submit" class="btn btn-danger" name="subscribe" id="subscribe">Subscribe</button>
            </form>
        
        </div>
    </div>
</div>
<input type="hidden" value="<?php echo $scholarID; ?>" name="scholarid" id="scholarid">
<hr style="width:100%;"></hr>
<table id="example" class="display table table-bordered table-hovered table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Article</th>
            <th>Citations</th> 
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $i= 1; foreach($ObjScholarArticles as $article): ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $article['ARTICLENAME']."<br/><em>".$article['JOURNAL']."</em>"; ?></td>
            <td><?php echo $article['CITATIONCOUNT']; ?></td>
            <td><form id="articledynamicsform">
                    <input type='hidden' value='<?php echo $article['ARTICLEID']; ?>' name='articleid'>
                    <input type='hidden' value='ArticleDynamics' name='action'>
                    <button type='submit' class='btn btn-default'><i class="fa fa-bar-chart-o modal-icon"></i></button>
                </form>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
<?php include "inc/footer.inc.php"; ?>
<div class="modal fade" id="articleDynamics" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" arial-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Article Dynamics</h4>
            </div>
            <div class="modal-body">
                <div id="articleDynamicsChart" class="articlechartdiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {        
        //get scholar IDs
        var str = document.getElementById('scholarid');
        var id = str.value;
        var objects = [];
        var objectsMostCited = [];
        
        //for article highchart
        $('form#articledynamicsform').submit(function(e){
            var objectArticleDyna = [];
            
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: "ajax/ajaxGET.php",
                dataType: "json",
                data: $( this ).serialize(),                //action specified in this form
                success: function(result){   
                    $.each(result, function (index, value) {
                        objectArticleDyna.push([value.month,value.count]);
                    });
                    console.log(objectArticleDyna);
                    //console.log(objectArticleDyna[0];
                    $('#articleDynamicsChart').highcharts({
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: 'Article Dynamic (A Year from now)'
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        },
                        yAxis: {
                            allowDecimals: false,
                            min: 0,
                            title: {
                                text: 'Citations'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        tooltip: {
                            pointFormat: 'Count: <b>{point.y} </b>'
                        },
                        series: [{
                            name: 'Population',
                            data: objectArticleDyna
                        }]
                    });
                    
                    var chart = $('#articleDynamicsChart').highcharts();
                    $('#articleDynamics').modal('show'); 
                    $('#articleDynamics').on('show.bs.modal', function() {
                      $('#articleDynamicsChart').css('visibility', 'hidden');
                    });
                    $('#articleDynamics').on('shown.bs.modal', function() {
                      $('#articleDynamicsChart').css('visibility', 'initial');
                       chart.reflow();
                    });                    
                }
            });
        });
        
        //articles datatables
        $('#example').DataTable({
            "aoColumnDefs":[
                {"bSortable": false, "aTargets":[ 0, 3 ]}
            ]
        });
        
        // Tab 1 article count
        $.ajax({
            type: "GET",
            url: "ajax/ajaxGET.php",
            data: {scholarid: id, action: "dynaCount"},
            success: function(result){                
                $("#count").html(result);
            }
          });
        
        //Tab 1 Chart  
        $.ajax({
            type: "GET",
            url: "ajax/ajaxGET.php",
            dataType: "json",
            data: {scholarid: id, action: "dynaDetails"},
            success: function(result){
                $.each(result, function (index, value) {
                    objects.push([value.articlename, value.diff]);
                });
                console.log(objects);
                $('#30daysDetails').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Citation Dynamics in 30Days'
                    },
                    tooltip: {
                        backgroundColor: {
                        linearGradient: [0,0,0,60],
                        stops: [
                            [0, '#FFFFFF'],
                            [0, '#E0E0E0']
                            ]
                        },
                        borderWidth: 1,
                        borderColor: '#AAA',
                        positioner: function () {
                            return { x: 650, y: 50 };
                        },
                        useHTML: true,
                        headerFormat: '<div class="tooltips"><strong>Publication</strong>',
                        pointFormat: '<br/><span class="pointname">{point.name}</span></br><br/><strong>Count</strong><br/>{point.y}',
                        footerFormat: '</div>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    var text = this.point.name,
                                    formatted = text.length > 25 ? text.substring(0, 25) + '...' : text;
                                    return '<div style="width:150px; overflow:hidden" title="' + text + '">' + formatted + '</div>';
                                }, 
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                        colorByPoint: true,
                            data: objects
                        }]
                    }); 
                }
        });
           
        //tab 2 chart
        $.ajax({
            type: "GET",
                url: "ajax/ajaxGET.php",
                dataType: "json",
                data: {scholarid: id, action: "dynaMostCited"},
                success: function(result){
                    $.each(result, function (index, value) {
                        objectsMostCited.push([value.articlename, value.diff]);
                    });
                    $('#mostCited').highcharts({
                        chart: {
                            type: 'pie'
                        },
                        title: {
                            text: '10 Most Cited Publication'
                        },
                        tooltip: {
                            pointFormat: '<br/>{point.name}</span></br><br/><strong>Count</strong><br/>{point.y}'
                        },
                        plotOptions: {
                            pie: {
                                innerSize: 280,
                                //allowPointSelect: true,
                                //cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    format: '<b><u>{point.name}</u></b>',
                                    //x: -5,
                                    style: {
                                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                                        width: '250px'
                                    }
                                }
                            }
                        },
                        series: [{
                            colorByPoint: true,
                            data: objectsMostCited
                        }]
                    });                     
                }
        });
        
        //Follow 
        $("#subs").submit(function(e){
            e.preventDefault();
            
            var str = document.getElementById('email');
            var userEmail = str.value;

            $.ajax({
                type: "GET",
                url: "ajax/ajaxGET.php",
                data: {scholarid: id, action: "Alert", email: userEmail},
                success: function(result){
                    console.log(result);
                    $("#thanks").html(result);
                }
            });
        });
      
        // Fix input element click problem
        $(' .dropdown-menu h5, .dropdown-menu button').click(function(e) {
            e.stopPropagation();
        });
        
        
    });
</script>