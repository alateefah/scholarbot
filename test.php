
<?php
include("inc/simple_html_dom.php");

$i = 1; $b = 20;
$link = 'https://scholar.google.co.uk/citations?user=WLOg-JgAAAAJ&hl=en';
$html = file_get_html($link);


while (!($html->find('td[class=gsc_a_e]'))){ //"No article" error div
    foreach ($html->find('tr[class=gsc_a_tr]') as $element):
        echo $i++;
        echo "<br>";
        
        // First td (contains publication, author(s) and journal)
        foreach($element->find('td[class=gsc_a_t]') as $first):
            foreach($first->find('a[class=gsc_a_at]') as $firstA):
                echo "Publication Name: ".$firstA->innertext."<br>";
            endforeach;
            foreach($first->find('div[class=gs_gray]') as $firstDiv):
                $fir = $firstDiv->find('span[class=gs_oph]', 0);
                echo $fir ? "Journal: ".$firstDiv->innertext."<br>" : "Author(s): ".$firstDiv->innertext."<br>";
            endforeach;
        endforeach;
        
        // Second td (contains Citation Count)
        foreach($element->find('td[class=gsc_a_c]') as $second):
            foreach($second->find('a[class=gsc_a_ac]') as $secondA):
                echo "Citation Count: ".$secondA->innertext."<br>";
            endforeach;
        endforeach;
        
        // Third td (contains published year)
        foreach($element->find('td[class=gsc_a_y]') as $third):
            echo "Year: ".$third->innertext."<br>";
        endforeach;
        
        echo "<br><br>";
    endforeach;
    $addToLink = '&cstart='.$b.'&pagesize=20';
    $link2 = $link.$addToLink;
    $html = file_get_html($link2);
    $b = $b + 20;
}

?>