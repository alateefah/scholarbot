<?php

function addScholar($sname,$link ){
    $Object = new DB2Connection();
    $outparam = "outparam";
    try {
        $result = $Object->procCall2('addScholar', "'$sname','$link',?",$outparam);
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function addArticle($scholarID,$articleName,$journal,$citation){
    $Object = new DB2Connection();

    try {
        $result = $Object->procCall('addArticle', "'$scholarID','$articleName','$journal','$citation'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function getScholars(){
    $Object = new DB2Connection();

    try {
        $result = $Object->procCall('getScholars', "");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function getScholar($scholarid){
    $Object = new DB2Connection();

    try {
        $result = $Object->procCall('getScholar', "'$scholarid'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function addTempArticle($scholarID,$articleName,$journal,$citation){
    $Object = new DB2Connection();

    try {
        $Object->procCall('addTempArticle', "'$scholarID','$articleName','$journal','$citation'");
        $result = "success";
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function compare($scholarID){
    $Object = new DB2Connection();

    try {
        $Object->procCall('compareArticles', "'$scholarID'");
        $result = "comparism success";
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function getScholarArticles($scholarID){
    $Object = new DB2Connection();

    try {
        $result = $Object->procCall('getScholarArticles', "'$scholarID'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}

function get30DaysDynamicsCount($scholarID){
    $Object = new DB2Connection();
    try {
        $result = $Object->procCall('get30DaysDynamicsCount', "'$scholarID'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}
function get30DaysDynamicsDetails($scholarID){
    $Object = new DB2Connection();
    try {
        $result = $Object->procCall('get30DaysDynamicDetails', "'$scholarID'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}
function get30DaysMostCited($scholarID){
    $Object = new DB2Connection();
    try {
        $result = $Object->procCall('get30DaysMostCited', "'$scholarID'");
    } catch (exception $e) {
        $result = array("status" => $e->getCode(), "error_msg" => $e->getMessage());
    }
    return $result;
}
function addAlert($scholarID, $email){
    $scholarInfo = call_user_func('getScholar',$scholarID);
    foreach($scholarInfo as $scholar):
        $a = $scholar['SCHOLARNAME'];
        $b = $scholar['SCHOLARURL'];
        $c = $scholar['SCHOLARID'];
    endforeach;
    $Object = new DB2Connection();
    try {
        $Object->procCall('addAlert', "'$scholarID','$email'");

        require '../PHPMailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com;';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'queenlattie720@gmail.com';                 // SMTP username
        $mail->Password = 'springproperty';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

        $mail->From = 'scholarbot@lateepha.org.ng';
        $mail->FromName = 'Scholarbot Subscription';
        $mail->addAddress($email);     // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        $mail->WordWrap = 50;                                 // Set word wrap to 50 characters
        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Citations Dynamics Tracking application';
        $mail->Body   = '<br/>You have subscribed to monthly overview of <b>'. $a .'</b>.<br/>  '
                . 'Please confirm your subscription by clicking on the following link'
                . ' <a href="scholarbot.mybluemix.net/?action=confirm&email='. $email.'&scholar='.$c.'">'
                . 'confirm cubscription</a>.<br/> <br/> You do not need to perform any action if you did not subscribe.';
        $mail->AltBody = '<br/>You have subscribed to monthly overview of <b>'. $a .'</b>.<br/>  '
                . 'Please confirm your subscription by clicking on the following link'
                . ' <a href="scholarbot.mybluemix.net/?action=confirm&email='. $email.'&scholar='.$c.'">'
                . 'confirm cubscription</a>.<br/> <br/> You do not need to perform any action if you did not subscribe.';

        if(!$mail->send()) {
            $result = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
        } else {
            $result = "Thanks for Subscribing. Message has been sent";
        }

        //$result = "Thanks for Subscribing";
    } catch (exception $e) {
        $result = $e->getMessage();
    }
    return $result;
}
function confirmSubscription($scholarID, $email){
    $Object = new DB2Connection();
    try {
        $result = $Object->procCall('confirmsubscription', "'$scholarID', '$email'");
    } catch (exception $e) {
        $result = $e->getMessage();
    }
    return $result; 
}
function deleteAlert($scholarID, $email){
    $Object = new DB2Connection();
    try {
        $result = $Object->procCall('deleteAlert', "'$scholarID', '$email'");
    } catch (exception $e) {
        $result = $e->getMessage();
    }
    return $result; 
}
function getArticleDynamics($articleid){
   $Object = new DB2Connection();
    try {
        $result = $Object->procCall('getArticleDynamics', "'$articleid'");
    } catch (exception $e) {
        $result = $e->getMessage();
    }
    return $result; 
}
?>
