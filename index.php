<?php
include("inc/simple_html_dom.php");
include ("connection/DB2Connection.php");
include ("class/class.crawler.php");

$scholars = new Scholar();
$ObjGetScholars = $scholars->getScholars();

//$ObjGetScholars = call_user_func('getScholars');
//var_dump($ObjGetScholars);

//action to confirm and delete subscription from URL
if(isset($_GET['action'])):                 //determine wihich action 
    $email = $_GET['email'];                //get the email from URL
    $scholarid = $_GET['scholar'];                            //get the scholarID
    
    $alert = new Alert($scholarid, $email);
    
    if($_GET['action'] === 'confirm'){                                          //if URL action is confirm  
        $ObjConfirmSubscription = $alert->confirmSubscription();                
    } else if ($_GET['action'] === 'unsubscribe'){                              //if URL action is unsubscribe
        $ObjUnsubscribe = $alert->deleteAlert();
    }
endif;

//include general header for the whole application
include 'inc/header.inc.php';
?>

<!-- Display Error in displaying all scholars --> 
<?php if(in_array("error_msg", $ObjGetScholars)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjGetScholars["error_msg"]; ?></div>
<?php endif;?>

<!-- Display Error in getting scholar details --> 
<?php if(isset($ObjGetScholar)): if(in_array("error_msg", $ObjGetScholar)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjGetScholar["error_msg"]; ?></div>
<?php endif; endif;?>

<!-- Display Error in Unsubscribing --> 
<?php if(isset($ObjConfirmSubscription)): ?>
    <div class="alert alert-warning" role="alert"><?php echo $ObjConfirmSubscription; ?></div>
<?php endif; ?> 
 
<!-- Display Error in Unsubscribing -->    
<?php if(isset($ObjUnsubscribe)): ?>
    <div class="alert alert-warning" role="alert"><?php echo $ObjUnsubscribe; ?></div>
<?php endif;?> 

<div class="col-md-9" style="border-right: 1px solid #CCC; border-bottom: 1px solid #CCC;">
    <div class="clearfix">&nbsp;</div>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">New URL</button>

    <div class="clearfix">&nbsp;</div>
    
    <!-- table to display all scholars -->
    <table id="example" class="table table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Scholar Name</th>
                <th>Total Articles</th>
                <th>Total Citations</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach($ObjGetScholars as $scholars): ?>
            <tr>
                <td><?php echo $i++; ?></td>
                <td><a href="<?php echo $scholars['SCHOLARURL']; ?>" target="_BLANK"><?php echo $scholars['SCHOLARNAME']; ?></a></td>
                <td><?php echo $scholars['TOTALARTICLES'];?></td>
                <td><?php echo $scholars['TOTALCITATIONS'];?></td>
                <td><form method='post' action='profile.php' >
                        <input type="hidden" value="<?php echo $scholars['SCHOLARID']; ?>" name='scholarid'>
                        <button type='submit' class='btn btn-default'>Profile</button>
                    </form>
                    
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <p>&nbsp;</p>
</div>
<div class="col-md-3" style="padding-top: 10px;">
    
    <img src="img/gsc_beta_logo.png" alt="Google Scholar Beta"/>
    <p>&nbsp;</p>
    
    <form class="form-inline" action="http://scholar.google.com/scholar" method="get" target="_blank" style="width: 120%;">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="q" class="form-control" id="search" placeholder="Search Google Scholar">
                <span class="input-group-btn">
                    <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
        <br/><br/>
        <button type="submit" class="btn btn-primary">Search</button>
    </form>
</div>
<?php include "inc/footer.inc.php"; ?>
<!-- Modal to add new profile-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New URL</h4>
      </div>
      <div class="modal-body">
          <form action="searchURL.php" method="post" id="newURL" class="form-horizontal">
                <div class="form-group">
                    <label for="scholarURL" class="col-sm-3">URL</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="URL" name="scholarURL">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-8">
                        <button type="submit" class="btn btn-primary" name="searchURL">Get Scholar Data</button>
                    </div>
                </div>
          </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    //poulate dataTable
    $('#example').DataTable({
        "aoColumnDefs":[
            {"bSortable": false, "aTargets":[ 0, 4 ]}
        ]
    });
    
    //new url action
    $('#searchURL').on('click', function(e){
        e.preventDefault();
        $.ajax({
            url: 'searchURL.php', //this is the submit URL
            type: 'POST', //or POST
            data: $('#newURL').serialize()
        });
    });
});
</script>