-- DB Script for Google Scholar citation tracker web application
-- Lateefah Abdulkareem
-- queenlattie720@gmail.com

--#SET TERMINATOR @
-- create schema crawler@
SET SCHEMA crawler@

DROP TABLE frequency@
CREATE TABLE frequency(
	frequencyID INT generated always AS identity PRIMARY KEY NOT NULL,
	descr VARCHAR(15) NOT NULL)@

DROP TABLE scholar@
CREATE TABLE scholar(
	scholarID INT generated always AS identity primary key NOT NULL,
	frequencyID INT NOT NULL DEFAULT 1,
	scholarName VARCHAR(255) NOT NULL,
	scholarURL VARCHAR(255) unique NOT NULL,
	createdDate TIMESTAMP,
	sys_start TIMESTAMP(12) generated always AS ROW BEGIN NOT NULL implicitly hidden,
	sys_end TIMESTAMP(12) generated always AS ROW END NOT NULL implicitly hidden,
	trans_start TIMESTAMP(12) generated always AS TRANSACTION START id implicitly hidden,
	period system_time(sys_start,sys_end),
	CONSTRAINT fk_scholar_frequencyID FOREIGN KEY(frequencyID) REFERENCES frequency(frequencyID))@

CREATE TABLE scholar_history LIKE scholar@
ALTER TABLE scholar_history append ON@
ALTER TABLE scholar ADD versioning USE history TABLE scholar_history@

DROP TABLE article@
CREATE TABLE article(
	articleID INT generated always AS identity PRIMARY KEY NOT NULL,
	scholarID INT NOT NULL,
	articleName VARCHAR(512) NOT NULL,
	journal VARCHAR(512),
	citationCount INT NOT NULL,
	crawlDate TIMESTAMP,
	sys_start TIMESTAMP(12) generated always AS ROW BEGIN NOT NULL implicitly hidden,
	sys_end TIMESTAMP(12) generated always AS ROW END NOT NULL implicitly hidden,
	trans_start TIMESTAMP(12) generated always AS TRANSACTION START id implicitly hidden,
	period system_time(sys_start,sys_end),
	CONSTRAINT fk_article_scholarID FOREIGN KEY (scholarID) REFERENCES scholar(scholarID))@

CREATE TABLE article_history LIKE article@
ALTER TABLE article_history append ON@
ALTER TABLE article ADD versioning USE history TABLE article_history@

DROP TABLE temp_article@
CREATE TABLE temp_article(
	articleID INT generated always AS identity PRIMARY KEY NOT NULL,
	scholarID INT NOT NULL,
	articleName VARCHAR(512) NOT NULL,
	journal VARCHAR(512),
	citationCount INT)@

DROP TABLE temp_difference@
CREATE TABLE temp_difference(
	scholarID INT NOT NULL,
	articleName VARCHAR(512) NOT NULL,
	journal VARCHAR(512),
	citationCount INT)@

DROP TABLE temp_article2@
CREATE TABLE temp_article2(
	articleID INT,
	scholarID INT NOT NULL,
	articleName VARCHAR(512) NOT NULL,
	journal VARCHAR(512),
	citationCount INT)@
INSERT INTO frequency (descr) VALUES ('Weekly'),('Bi-Monthly'),('Monthly')@
-- ------------------------------------------------------------------------------------
-- PROCEDURES
-- ------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE getfrequency()
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		SELECT * FROM frequency ORDER BY descr;
	OPEN C;
END@
-- -----------------------
CREATE OR REPLACE PROCEDURE addScholar(IN in_frequencyID anchor frequency.frequencyID, IN in_scholarName anchor scholar.scholarName, 
					IN in_scholarURL anchor scholar.scholarURL, OUT out_scholarID anchor scholar.scholarID)
BEGIN
	
	INSERT INTO scholar (frequencyID, scholarName, scholarURL, createdDate) VALUES (in_frequencyID, in_scholarName, in_scholarURL, CURRENT TIMESTAMP);
	set out_scholarID = IDENTITY_VAL_LOCAL();
END@
-- ------------------------
CREATE OR REPLACE PROCEDURE addArticle(IN in_scholarID anchor scholar.scholarID, IN in_articleName anchor article.articleName, 
					IN in_journal anchor article.journal, IN in_citationCount anchor article.citationCount)
BEGIN
	INSERT INTO article (scholarID, articleName, journal, citationCount,crawlDate) VALUES
		(in_scholarID, in_articleName, in_journal, in_citationCount,CURRENT TIMESTAMP);
END@
-- ----------------------------------------

CREATE OR REPLACE PROCEDURE getScholars()
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		SELECT s.scholarID, s.frequencyID, s.scholarName, s.scholarURL, 
		s.createdDate, count(a.scholarID) as totalArticles,
		sum(a.citationCount) as totalCitations
		FROM scholar s
		JOIN article a on s.scholarID = a.scholarID
		where s.scholarID = a.scholarID
		group by s.scholarID, s.frequencyID, s.scholarName, s.scholarURL, 
		s.createdDate; 
	OPEN C;
END@
-- -----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE getScholar(IN in_scholarID anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		SELECT * FROM scholar where scholarID=in_scholarID;
	OPEN C;	
END@
-- --------------------------------------------------------------------------
create or replace procedure getScholarArticles(IN in_scholarID anchor scholar.scholarID)
begin
	declare c cursor with return to client for
		select * from article where scholarid=in_scholarID;
	open c;
end@
-- ------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE addTempArticle(IN in_scholarID anchor scholar.scholarID, IN in_articleName anchor article.articleName, 
					IN in_journal anchor article.journal, IN in_citationCount anchor article.citationCount)
BEGIN
	INSERT INTO temp_article (scholarID, articleName, journal, citationCount) VALUES
		(in_scholarID, in_articleName, in_journal, in_citationCount);
END@
-- ---------------------------------------------

CREATE OR REPLACE PROCEDURE compareArticles(IN in_scholarID anchor scholar.scholarID)
BEGIN
	delete from temp_difference;
	-- get differences an store
	insert into temp_difference(scholarID,articleName,journal,citationCount)
		SELECT scholarID,articleName,journal,citationCount FROM article where scholarID=in_scholarID
				EXCEPT ALL SELECT scholarID,articleName,journal,citationCount FROM temp_article where scholarID=in_scholarID
			UNION ALL
		SELECT scholarID,articleName,journal,citationCount FROM temp_article where scholarID=in_scholarID
				EXCEPT ALL SELECT scholarID,articleName,journal,citationCount FROM article where scholarID=in_scholarID;
	-- Merge differences
	MERGE INTO article ar  
	USING (select scholarID,articleName,journal,citationCount FROM temp_difference) td  
	ON (ar.scholarID = td.scholarID and  ar.articleName=td.articleName and ar.journal=td.journal)
	WHEN MATCHED THEN
		UPDATE SET citationCount = td.citationCount
	WHEN NOT MATCHED THEN
		INSERT (scholarID,articleName,journal,citationCount, crawlDate) VALUES (td.scholarID,td.articleName,td.journal,td.citationCount, CURRENT TIMESTAMP);
	delete from temp_article;
END@
-- -----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE get30DaysDynamicsCount(IN in_scholarID anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarID 
			except all 
		select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarID;
	OPEN C;
end@
-- -----------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE get30DaysDynamicDetails(IN in_scholarid anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
        select articleid, max(citationCount) - min(citationCount) as difference, articlename, journal
                from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where articleid in
             (select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarid
                except all
               select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarid)
                group by articleid,articlename, journal;
        OPEN C;
end@
-- -------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE get30DaysMostCited(IN in_scholarid anchor scholar.scholarID)
BEGIN
    DECLARE C CURSOR WITH RETURN TO CLIENT FOR
        select articleid, max(citationCount) - min(citationCount) as difference, articlename, journal
            from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where articleid in
                (select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarid
                     except all
                    select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarid)
		
		group by articleid,articlename, journal 
		order by difference desc
		fetch first 10 rows only;
    OPEN C;
end@