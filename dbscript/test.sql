--#SET TERMINATOR @
-- create schema crawler@
SET SCHEMA crawler@

-- ------------------------------------------------------------------------------------
-- PROCEDURES
-- ------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE addScholar(IN in_scholarName anchor scholar.scholarName, 
					IN in_scholarURL anchor scholar.scholarURL, OUT out_scholarID anchor scholar.scholarID)
BEGIN
	
	INSERT INTO scholar (scholarName, scholarURL, createdDate) VALUES (in_scholarName, in_scholarURL, CURRENT TIMESTAMP);
	set out_scholarID = IDENTITY_VAL_LOCAL();
END@
-- ------------------------
CREATE OR REPLACE PROCEDURE addArticle(IN in_scholarID anchor scholar.scholarID, IN in_articleName anchor article.articleName, 
					IN in_journal anchor article.journal, IN in_citationCount anchor article.citationCount)
BEGIN
	INSERT INTO article (scholarID, articleName, journal, citationCount,crawlDate) VALUES
		(in_scholarID, in_articleName, in_journal, in_citationCount,CURRENT TIMESTAMP);
END@
-- ----------------------------------------

CREATE OR REPLACE PROCEDURE getScholars()
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		SELECT s.scholarID, s.scholarName, s.scholarURL, 
		s.createdDate, count(a.scholarID) as totalArticles,
		sum(a.citationCount) as totalCitations
		FROM scholar s
		JOIN article a on s.scholarID = a.scholarID
		where s.scholarID = a.scholarID
		group by s.scholarID, s.scholarName, s.scholarURL, 
		s.createdDate; 
	OPEN C;
END@
-- -----------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE getScholar(IN in_scholarID anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		SELECT * FROM scholar where scholarID=in_scholarID;
	OPEN C;	
END@
-- --------------------------------------------------------------------------
create or replace procedure getScholarArticles(IN in_scholarID anchor scholar.scholarID)
begin
	declare c cursor with return to client for
		select * from article where scholarid=in_scholarID;
	open c;
end@
-- ------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE addTempArticle(IN in_scholarID anchor scholar.scholarID, IN in_articleName anchor article.articleName, 
					IN in_journal anchor article.journal, IN in_citationCount anchor article.citationCount)
BEGIN
	INSERT INTO temp_article (scholarID, articleName, journal, citationCount) VALUES
		(in_scholarID, in_articleName, in_journal, in_citationCount);
END@
-- ---------------------------------------------

CREATE OR REPLACE PROCEDURE compareArticles(IN in_scholarID anchor scholar.scholarID)
BEGIN
	delete from temp_difference;
	-- get differences an store
	insert into temp_difference(scholarID,articleName,journal,citationCount)
		SELECT scholarID,articleName,journal,citationCount FROM article where scholarID=in_scholarID
				EXCEPT ALL SELECT scholarID,articleName,journal,citationCount FROM temp_article where scholarID=in_scholarID
			UNION ALL
		SELECT scholarID,articleName,journal,citationCount FROM temp_article where scholarID=in_scholarID
				EXCEPT ALL SELECT scholarID,articleName,journal,citationCount FROM article where scholarID=in_scholarID;
	-- Merge differences
	MERGE INTO article ar  
	USING (select scholarID,articleName,journal,citationCount FROM temp_difference) td  
	ON (ar.scholarID = td.scholarID and  ar.articleName=td.articleName and ar.journal=td.journal)
	WHEN MATCHED THEN
		UPDATE SET citationCount = td.citationCount
	WHEN NOT MATCHED THEN
		INSERT (scholarID,articleName,journal,citationCount, crawlDate) VALUES (td.scholarID,td.articleName,td.journal,td.citationCount, CURRENT TIMESTAMP);
		-- call crawler.addArticle(td.scholarID,td.articleName,td.journal,td.citationCount);
	delete from temp_article;
END@
-- -----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE get30DaysDynamicsCount(IN in_scholarID anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarID 
			except all 
		select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarID;
	OPEN C;
end@
-- -----------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE get30DaysDynamicDetails(IN in_scholarid anchor scholar.scholarID)
BEGIN
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
        select articleid, max(citationCount) - min(citationCount) as difference, articlename, journal
                from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where articleid in
             (select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarid
                except all
               select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarid)
                group by articleid,articlename, journal;
        OPEN C;
end@
-- -------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE get30DaysMostCited(IN in_scholarid anchor scholar.scholarID)
BEGIN
    DECLARE C CURSOR WITH RETURN TO CLIENT FOR
        select articleid, max(citationCount) - min(citationCount) as difference, articlename, journal
            from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where articleid in
                (select articleid from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY where scholarid = in_scholarid
                     except all
                    select distinct(articleid) from article for SYSTEM_TIME FROM CURRENT DATE - 30 DAYS TO CURRENT DATE + 1 DAY  where scholarid = in_scholarid)
		
		group by articleid,articlename, journal 
		order by difference desc
		fetch first 10 rows only;
    OPEN C;
end@
-- ---------------------------------------------------------------------------------------------------------------	
create or replace procedure addAlert(IN in_scholarID anchor scholar.scholarID, IN in_email anchor alert.email)
begin
	insert into alert(scholarID, email) values (in_scholarID, in_email);
end@
-- ---------------------------------------------------------------------------------------------------------------
create or replace procedure deleteAlert(IN in_scholarID anchor scholar.scholarID, IN in_email anchor alert.email)
begin
	delete from alert where scholarID = in_scholarID and email=in_email;
end@
-- -------------------------------------------------------------------------------------------------------------
create or replace procedure confirmsubscription(IN in_scholarID anchor scholar.scholarID, IN in_email anchor alert.email)
begin
	if (select count(*) from alert where scholarid = in_scholarID and email = in_email and status = 1) <> 0 then
		SIGNAL SQLSTATE '90003' SET MESSAGE_TEXT='You are already subscribed to this';	
	else			
		update alert set status=1 where scholarID = in_scholarID and email=in_email;
	end if;
end@
-- ------------------------------------------------------------------------------------------------
create or replace procedure getArticleDynamics(IN in_articleid anchor article.articleid)
begin
	DECLARE C CURSOR WITH RETURN TO CLIENT FOR
		select max(citationCount) - min(citationCount) as articleCount, monthname(sys_start) as month from article for system_time from current timestamp - 1 year to current timestamp + 1 day where articleid=in_articleid group by monthname(sys_start);
	OPEN C;
end@