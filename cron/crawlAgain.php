<?php

//attach files, html_dom, dbconnect and func file
include("../inc/simple_html_dom.php");
include ("../connection/DB2Connection.php");
include ("../class/class.crawler.php");

//get all scholar in database
$scholarss = new Scholar();
$allScholars = $scholarss->getScholars();

foreach ($allScholars as $ascholar):
    $a = $ascholar['SCHOLARID'];
    $b = $ascholar['SCHOLARURL'];
    $scholars[] = array("scholarid"=>$a,"scholarurl"=>$b);
endforeach;
//var_dump($scholars);
$i = 0;

foreach ($scholars as $sch):
    $i++;
    echo "Starting ".$i." <br/>";
    $scholarID = $sch['scholarid'];
    $scholarURL = $sch['scholarurl'];
    echo "ScholarID: ".$scholarID."<br/>";
    
    $scholarProfile = file_get_html($scholarURL);           //find the profile
    
    if($scholarProfile->find('div[id=gsc_prf_i]')){         //if profile exist
        $crawler = new Crawler($scholarURL);
        $publications = $crawler->crawlLink();
                
        
        //insert into temp article table
        if(isset($publications)): 
            foreach($publications as $publication):
                $pName = $publication['publicationName'];
                $pJournal = $publication['publicationJournal'];
                $pCitation = intval($publication['citationCount']);
                
                $article = new Article();
                
                $article->setScholarID($scholarID);
                $article->setArticleDetails($pName, $pJournal, $pCitation);
            
                $ObjAddTemp = $article->addTempArticle();
                  var_dump($ObjAddTemp);
            endforeach;
        endif;
        
        $scholar = new Scholar();
        $scholar->setScholarID($scholarID);
        $ObjCompare =  $scholar->compare();
            var_dump($ObjCompare);
        echo "Ending ".$i." <br/><br/>";
        unset($publications);
    } else {
        $ObjCrawlAgainStatus = "Profile no longer exist, please check link entered";
    }
  
endforeach;  

