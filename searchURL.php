<?php
include("inc/simple_html_dom.php");
include ("connection/DB2Connection.php");
include ("class/class.crawler.php");

/*
 * Search if profile exists
 */
if(isset($_POST['searchURL'])){
    $link = $_POST['scholarURL'];
    $scholarHTML = file_get_html($link);
    if($scholarHTML->find('div[id=gsc_prf_i]')){
        echo "<script type='text/javascript'> $('#myModal').modal('show'); </script>";
    } else {
        echo "Profile does not exist, please check link entered";
    }  
} 

/*
 * If Wrong Profile info
 */
if(isset($_POST['no'])):
     header("location:index.php");              //return to homepage
endif;

/*
 * if user does not agree with terms and codition of use of application
 */
if(isset($_POST['cancelCrawl'])):
     header("location:index.php");
endif;

/*
 * If User accepts terms and condition for use of application
 */
if(isset($_POST['crawlProfile'])){
    $sname = $_POST['scholarName'];
    $link = $_POST['link'];
    
    $scholar = new Scholar();                                         //create a Scholar Object
    $outparam = $scholar->addScholar($sname, $link);                  //that adds scholar to database
    $scholarID =  $outparam[0];                                        //the database returns scholarid
    
    $crawler = new Crawler($link);                      //create crawler object
    $publications = $crawler->crawlLink();              //crawls the scholar's page and stor store publications in a vaiabe
    
    

    //insert into article table
    if(isset($publications)): 
        foreach($publications as $publication):
            $pName = $publication['publicationName'];
            $pJournal = $publication['publicationJournal'];
            $pCitation = $publication['citationCount'];
            
            $article = new Article();
            $article->setScholarID($scholarID);
            $article->setArticleDetails($pName, $pJournal, $pCitation);
            
            $ObjAddArticle = $article->addArticle();
            //$ObjAddArticle = call_user_func("addArticle",$scholarID, $pName, $pJournal, $pCitation);
        endforeach;
    endif;
    unset($publications);
    header("location:index.php");
}
include 'inc/header.inc.php';

?>
        <div class="container">
            <div class="clearfix">&nbsp;</div>
            
            <?php if(isset($outparam)): if(in_array("error_msg", $outparam)):?>
                <div class="alert alert-danger" role="alert"><?php echo $outparam["error_msg"]; ?></div>
            <?php endif; endif;?>
                
            <?php if(isset($ObjAddArticle)): if(in_array("error_msg", $ObjAddArticle)):?>
                <div class="alert alert-danger" role="alert"><?php echo $ObjAddArticle["error_msg"]; ?></div>
            <?php endif; endif;?>
                
            <?php 
                if(isset($scholarHTML)):
                    if($scholarHTML->find('div[id=gsc_prf_i]')):        //search for profile
                        echo "Is this your page??? <br><br>";
                        foreach ($scholarHTML->find('div[id=gsc_prf_i]') as $element):
                            foreach($element->find('div[id=gsc_prf_in]') as $name):
                                $scholarName = "Profile Name: ".$name->innertext."<br>";
                                echo $scholarName;
                            endforeach;
                            foreach($element->find('div[class=gsc_prf_il]') as $profileType):
                                echo "Profile Information: ".$profileType->innertext."<br>";
                            endforeach;
                        endforeach;
                        echo '<form class="form-horizontal" method="post" action="searchURL.php">';
                        echo '<button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirmModal">Yes</button>';
                        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        echo '<button type="submit" class="btn btn-danger" name="no">No</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                        echo '</form>';
                    endif;
                endif;
            ?>
        </div>
    </body>
  </head>
</html>
<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Crawl Profile</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" method="post" name="crawl" id="crawlForm" action="searchURL.php">
              <?php
                foreach ($scholarHTML->find('div[id=gsc_prf_i]') as $element):
                    foreach($element->find('div[id=gsc_prf_in]') as $name):
                        $scholarName = $name->innertext;
                    endforeach;
                endforeach; ?>
              <input type="hidden" value="<?php echo $link;?>" name="link">
              <div class="form-group">
                    <label for="scholarName" class="col-sm-3"> Profile Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="scholarName" value="<?php echo $scholarName; ?>" readonly="readonly">
                    </div>
              </div>              
              <div class="form-group">
                <div class="col-sm-11">
                  <label>
                      <input type="checkbox" required> Accept all <a href="">Terms and Conditions</a> for using this application.
                  </label>
                </div>
              </div>
              <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-7">
                        <button type="submit" class="btn btn-primary" name="crawlProfile">Crawl My Page</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
             </div>
          </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    // bind 'myForm' and provide a simple callback function
    $('#crawlForm').ajaxForm(function() {
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serializeArray()
    });
});
</script>