<?php


error_reporting(E_ALL ^ E_WARNING);

class DB2Connection {
	private $hostname  = "localhost";
	private $username  = "db2admin";
	private $password  = "password";
	private $database  = "CRAWLER4";
        /*
        private $hostname  = "quanteq.com";
	private $username  = "alattie";
	private $password  = "PZ6E5U4g";
	private $database  = "QRSDG";
	*/
	private $port = 50000;
	private $properties = array();
	protected $conn = false;
	protected $output = array();
	protected $row;
	
	function __construct() {
		$dsn = "DATABASE=$this->database;HOSTNAME=$this->hostname;PORT=$this->port;
  			PROTOCOL=TCPIP;UID=$this->username;PWD=$this->password;";
		$this->conn = db2_pconnect($dsn, NULL, NULL);
		if (!$this->conn){
			throw new Exception("Cannot connect to server: " .db2_conn_errormsg(). " [sqlcode " . 
				db2_conn_error(). "]");
		}else{
			try{
				$sql = "SET PATH=CURRENT PATH, CRAWLER";
				$ret = db2_exec($this->conn, $sql);
			}catch(exception $e){
                            throw new Exception("Paath and Sql Error: ".$e->getMessage());
				//var_dump($e); exit();
			}//throw new Exception("Error XX Message: ".db2_stmt_errormsg());
		}		
	}

	function __call($method,$params){
		throw new Exception("the method {$method} does not exist");
    }
   
   function __get($propertyname){
		return $this->properties[$propertyname];
	}
    
	function __set($propertyname,$value){
		$this->properties[$propertyname] = $value;
	}

	function getConnection() {
		return $this->conn;
	}
	
	
	function procCall($procName, $params=""){
#		var_dump(self::$conn); exit();
		if(!$this->conn){
			throw new Exception("The DB2 connection is invalid");
		}

		$sql = "CALL {$procName}({$params});";
		$ret = db2_exec($this->conn, $sql);
   	if($ret){
			$this->output = array();
			while($this->row = db2_fetch_assoc($ret)){
   			$this->output[] = $this->row;
   		} 
         return $this->output;
   	}
   	throw new Exception("Error Message: ".db2_stmt_errormsg());
	}
	
	function procCallEx($procName, $params=""){
			// returns 2 result sets
		if(!$this->conn){
			throw new Exception("The DB2 connection is invalid");
		}
		$sql = "CALL {$procName}({$params});";
		$ret = db2_exec($this->conn, $sql);
#		var_dump(self::$conn, $sql, $ret); exit();
		if($ret){
			$this->output = array();
			$this->output[0] = array();
			while($this->row = db2_fetch_assoc($ret)){
				$this->output[0][] = $this->row;
			}
			$res = db2_next_result($ret);
			if($res){
				$this->output[1] = array();
				while($this->row = db2_fetch_assoc($res)){
					$this->output[1][] = $this->row;
				}
			}
			return $this->output;
		}
		throw new Exception("Error Message: ".db2_stmt_errormsg());
	}

	function procCallPic($proc, $params, &$fileparam){
		//for picture files
		if(!$this->conn){
			throw new Exception("The DB2 connection is invalid");
		}else{
			$sql = "CALL {$proc}({$params})";
			$stmt = db2_prepare($this->conn, $sql);
			if($stmt){
				db2_bind_param($stmt, 1, $fileparam, DB2_PARAM_FILE);	
				$res = db2_execute($stmt);
				if($res){
					return $res;
				}else{
					throw new Exception("Error Message: ".db2_stmt_errormsg());
				}
			}else{
				throw new Exception("Error Message: ".db2_stmt_errormsg());
			}
		}
	}

	function procCall2($procName, $params, &$outparam){
		// has one OUT param
		if(!$this->conn){
			throw new Exception("The DB2 connection is invalid");
		}else{
			$sql = "CALL {$procName}({$params})";
			$stmt = db2_prepare($this->conn, $sql);
			if($stmt){
				db2_bind_param($stmt, 1, $outparam, DB2_PARAM_OUT);
				$res=db2_execute($stmt);
				if($res){
					$this->output = array();
					$this->output[] = $outparam;
					while($this->row = db2_fetch_assoc($res)){
						$this->output[] = $this->row;
					}
					return $this->output;
				}else{
					throw new Exception("Error Message: ".db2_stmt_errormsg());
				}
			}else{
				throw new Exception("Error Message: ".db2_stmt_errormsg());
			}
		}
	}
}


?>