<?php
include("inc/simple_html_dom.php");
include ("connection/DB2Connection.php");
include ("func/function.php");

$ObjGetScholars = call_user_func('getScholars');

if(isset($_POST['crawlagain'])):
    $scholarID = $_POST['scholarid'];
    $ObjGetScholar = call_user_func('getScholar',$scholarID);
    foreach($ObjGetScholar as $scholar):
        $scholarURL = $scholar['SCHOLARURL'];
    endforeach;
    
    $scholarHTML = file_get_html($scholarURL);
    if($scholarHTML->find('div[id=gsc_prf_i]')){
        $b = 20;
        while (!($scholarHTML->find('td[class=gsc_a_e]'))){                 //while not profile error
            foreach ($scholarHTML->find('tr[class=gsc_a_tr]') as $element): //table row for a particular publicatiion
                
                // First td (contains publication, author(s) and journal)
                foreach($element->find('td[class=gsc_a_t]') as $first):
                    foreach($first->find('a[class=gsc_a_at]') as $firstA):
                        $publicationName =  $firstA->innertext;
                    endforeach;
                    foreach($first->find('div[class=gs_gray]') as $firstDiv):
                        $fir = $firstDiv->find('span[class=gs_oph]', 0);
                        if($fir){ $journal = $firstDiv->innertext; }
                    endforeach;
                endforeach;

                // Second td (contains Citation Count)
                foreach($element->find('td[class=gsc_a_c]') as $second):
                    foreach($second->find('a[class=gsc_a_ac]') as $secondA):
                        $citationCount = empty($secondA->innertext)? "0":$secondA->innertext;
                    endforeach;
                endforeach;
                $publications[] = array("publicationName"=>$publicationName,"publicationJournal"=>$journal,
                        "citationCount"=>$citationCount);
            endforeach;
            $addToLink = '&cstart='.$b.'&pagesize=20';
            $link2 = $scholarURL.$addToLink;
            $scholarHTML = file_get_html($link2);
            $b = $b + 20;
        }   

    //insert into article table
    if(isset($publications)): foreach($publications as $publication):
            $pName = $publication['publicationName'];
            $pJournal = $publication['publicationJournal'];
            $pCitation = intval($publication['citationCount']);
            $result3 = call_user_func("addTempArticle",$scholarID, $pName, $pJournal, $pCitation);
    endforeach;endif;
            $result4 = call_user_func('compare',$scholarID);
            header("locaton:index.php");
    } else {
        $ObjCrawlAgainStatus = "Profile no longer exist, please check link entered";
    }
endif;

include 'inc/header.inc.php';
?>

<?php if(in_array("error_msg", $ObjGetScholars)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjGetScholars["error_msg"]; ?></div>
<?php endif;?>

<?php if(isset($ObjGetScholar)): if(in_array("error_msg", $ObjGetScholar)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjGetScholar["error_msg"]; ?></div>
<?php endif; endif;?>

<?php if(isset($ObjCrawlAgainStatus)): if(in_array("error_msg", $ObjCrawlAgainStatus)):?>
    <div class="alert alert-danger" role="alert"><?php echo $ObjCrawlAgainStatus["error_msg"]; ?></div>
<?php endif; endif;?>

<?php if(isset($result3)): if(in_array("error_msg", $result3)):?>
    <div class="alert alert-danger" role="alert"><?php echo $result3["error_msg"]; ?></div>
<?php endif; endif;?>    

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  New URL
</button>
<div class="clearfix">&nbsp;</div>
<table class="table table-striped table-bordered table-hover" id="table">
    <tr>
        <th>#</th>
        <th>id</th>
        <th>Scholar Name</th>
        <th>Total Articles</th>
        <th>Total Citations</th>
        <th colspan="2">Action</th>
    </tr> 
    <?php $i= 1;if(!in_array("error_msg", $ObjGetScholars)): foreach ($ObjGetScholars as $scholar): ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $scholar['SCHOLARID']; ?></td>
            <td><a href="<?php echo $scholar['SCHOLARURL']; ?>" target="_BLANK"><?php echo $scholar['SCHOLARNAME']; ?></a></td>
            <td><?php echo $scholar['TOTALARTICLES']; ?></td>
            <td><?php echo $scholar['TOTALCITATIONS']; ?></td>
            <td><form method="post" action="index.php">
                    <input type="hidden" value="<?php echo $scholar['SCHOLARID']; ?>" name="scholarid">
                    <button type="submit" class="btn btn-default" name="crawlagain">Crawl Again</button>
                </form>
            </td>
            <td><form method="post" action="profile.php" >
                    <input type="hidden" value="<?php echo $scholar['SCHOLARID']; ?>" name="scholarid">
                    <button type="submit" class="btn btn-default">Profile</button>
                </form>
            </td>
        </tr> 
    <?php endforeach; endif;?>
</table> -->
<table id="example" class="display table table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Scholar Name</th>
            <th>Total Articles</th>
            <th>Total Citations</th> 
            <th>Action</th>
        </tr>
    </thead>
</table>
<?php include "inc/footer.inc.php"; ?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">New URL</h4>
      </div>
      <div class="modal-body">
          <form action="searchURL.php" method="post" id="newURL" class="form-horizontal">
                <div class="form-group">
                    <label for="scholarURL" class="col-sm-3">URL</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="URL" name="scholarURL">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-8">
                        <button type="submit" class="btn btn-primary" name="searchURL">Get Scholar Data</button>
                    </div>
                </div>
          </form>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    //dataTable
    $('#example').DataTable({
        "ajax": "ajax/scholars.php"
    });
    
    $.ajax({
        type: "GET",
        url: "ajax/ajaxGET.php",
        data: {action: "Scholars"},
        success: function(result){
            console.log(result)
        },
        error: function(msg){
            console.log(msg);
        }
    });
    // bind 'myForm' and provide a simple callback function
    /*
    $('#newURL').ajaxForm(function() {
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serializeArray()
    });
    $('#recordProfile').ajaxForm(function() {
        type: 'POST',
        url: $(this).attr('action'),
        data: $(this).serializeArray()
    });
    */
});
</script>