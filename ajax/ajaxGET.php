<?php

include ("../connection/DB2Connection.php");
include ("../class/class.crawler.php");

$action = $_GET['action'];
$output = call_user_func($action);
echo $output;

function dynaCount(){
    $scholarid = $_GET['scholarid'];
    
    $scholar = new Scholar();
    $scholar->setScholarID($scholarid);
    
    $ObjDynaCount = $scholar->get30DaysDynamicsCount();
    return count($ObjDynaCount);
}

function dynaDetails(){
    $scholarid = $_GET['scholarid'];
    
    $scholar = new Scholar();
    $scholar->setScholarID($scholarid);
    
    $ObjDynaDetails = $scholar->get30DaysDynamicsDetails();
    foreach($ObjDynaDetails as $ObjDynaDetail):
        $a = $ObjDynaDetail['ARTICLEID'];
        $b = $ObjDynaDetail['DIFFERENCE'];
        $c = htmlspecialchars($ObjDynaDetail['ARTICLENAME']);
        $json[] = array("articleid"=>$a,"diff"=>$b,"articlename"=>$c);
    endforeach;
    header("Content-type: application/json");
    return json_encode($json);
}

function dynaMostCited(){
    $scholarid = $_GET['scholarid'];
    
    $scholar = new Scholar();
    $scholar->setScholarID($scholarid);
    
    $ObjDynaDetails = $scholar->get30DaysMostCited();
    foreach($ObjDynaDetails as $ObjDynaDetail):
        $a = $ObjDynaDetail['ARTICLEID'];
        $b = $ObjDynaDetail['DIFFERENCE'];
        $c = htmlspecialchars ($ObjDynaDetail['ARTICLENAME']);
        $json[] = array("articleid"=>$a,"diff"=>$b,"articlename"=>$c);
    endforeach;
    header("Content-type: application/json");
    return json_encode($json);
}


function Alert(){
    $scholarid = $_GET['scholarid'];
    $email = $_GET['email'];
    
    $alert = new Alert($scholarid, $email);
  
    $json = $alert->addAlert();
    if(db2_stmt_error()) {
        $json = db2_stmt_errormsg();
        //$json = "Error: You are already subscribed to alert";
    }
    header("Content-type: application/json");
    return json_encode($json);
}

function ArticleDynamics(){
    $articleid = $_GET['articleid'];
    $article = new Article();
    $article->setArticleID($articleid);
    $ObjArticleDynamics = $article->getArticleDynamics();
    foreach($ObjArticleDynamics as $articleDynamic):
        $a = $articleDynamic['ARTICLECOUNT'];
        $b = $articleDynamic['MONTH'];
        $c = $articleDynamic['ARTICLENAME'];
        $json[] = array("count"=>$a,"month"=>$b,"aname"=>$c);
    endforeach;
    header("Content-type: application/json");
    return json_encode($json);
}