<?php

include ("../connection/DB2Connection.php");
include ("../func/function.php");
$i = 1;
$ObjScholars = call_user_func('getScholars');
foreach($ObjScholars as $scholar):
    $counter = $i++;
    $scholarname = "<a href='".$scholar['SCHOLARURL']."' target='_BLANK'>".htmlspecialchars ($scholar['SCHOLARNAME'])."</a>";
    $articlescount = $scholar['TOTALARTICLES'];
    $citations = $scholar['TOTALCITATIONS'];
    $profile = "<form method='post' action='profile.php' ><input type='hidden' value='".
            $scholar['SCHOLARID']."' name='scholarid'><button type='submit' class='btn btn-default'>Profile</button></form>";
    $json[] = array($counter,$scholarname,$articlescount, $citations,$profile);
endforeach;
header("Content-type: application/json");
echo json_encode(array("data"=>$json));

